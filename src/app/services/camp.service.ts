import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, ObservedValueOf } from 'rxjs';
import { Camp } from '../interfaces/camp';
import { CampRegistration } from '../interfaces/campRegistration';
import { CampRegistrationResult } from '../interfaces/campRegistrationResult';
import { CampResult } from '../interfaces/campresult';
import { Config } from "./api-config";

@Injectable({providedIn: 'root'})
export class CampService {
    constructor(private http: HttpClient) { }  




    private campUrl = Config.API_BASE_URL + '/camps';

    private httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
        })
      };


    public getAllCamps(): Observable<Camp[]>{
        return this.http.get<Camp[]>(this.campUrl, this.httpOptions);
    }

    public registerToCamp(data: CampRegistration): Observable<any>{
        return this.http.post<CampRegistrationResult>(this.campUrl + '/register', data,this.httpOptions);
    }


}