import { Routes } from "@angular/router";
import { CampOverview } from "./componenten/camp/overview/camp-overview.component";
import { CampRegistration } from "./componenten/camp/registration/camp-registration.component";
import { IndexComponent } from "./componenten/index/index.component";
import { LoginComponent } from "./componenten/login/login.component";

export const AppRoutes: Routes = [
    { path: '', redirectTo: '/ktcb/inschrijven', pathMatch: 'full' },
    { path: 'index', component: IndexComponent},
    { path: 'login', component: LoginComponent},
    { path: 'ktcb/inschrijven', component: CampOverview},
    { path: 'ktcb/registratie/:campId', component: CampRegistration}
]