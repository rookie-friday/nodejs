import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { RouterModule } from "@angular/router";
import { HttpClientModule } from '@angular/common/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
//Components
import { AppComponent } from "./app.component";
import { LoginComponent } from "./componenten/login/login.component";

//Routes
import { AppRoutes } from "./app.routes";
import { CampRegistration } from "./componenten/camp/registration/camp-registration.component";
import { CampOverview } from "./componenten/camp/overview/camp-overview.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

@NgModule({
	imports: [
		BrowserModule,
		RouterModule.forRoot(AppRoutes),
		HttpClientModule,
		FormsModule, 
		ReactiveFormsModule
	],
	declarations: [
		//Components
		AppComponent,
		LoginComponent,
		CampRegistration,
		CampOverview
	],
	providers: [ 
	],
	bootstrap: [AppComponent]
})

export class AppModule {
}