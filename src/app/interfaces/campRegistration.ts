export interface CampRegistration{
    groupId: number,
    levelId: number,
    name: string
}