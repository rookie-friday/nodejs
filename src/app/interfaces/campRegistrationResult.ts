export interface CampRegistrationResult{
    success: boolean,
    message: string
}