export interface Camp{
    id: number,
    title: string, 
    startTime: string,
    endTime: string
    image: string,
    capacity: number,
    members: number,
    date: string
}