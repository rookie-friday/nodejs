import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'webdream-index',
    templateUrl: 'index.component.html'
})

export class IndexComponent implements OnInit {
    constructor(private router: Router) { }

    ngOnInit() { }



    public navigateToPage(pageName: string): void {
        this.router.navigate([`/${pageName}`]);
    }
}