import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { CampRegistrationResult } from 'src/app/interfaces/campRegistrationResult';
import { CampService } from 'src/app/services/camp.service';

@Component({
    selector: 'camp-registration',
    templateUrl: 'camp-registration.component.html',
    styleUrls: ['./camp-registration.scss']
})

export class CampRegistration implements OnInit {
    constructor(private fb: FormBuilder,
        private route: ActivatedRoute, private campService: CampService) { }

    public myForm: FormGroup
    public saved: boolean = false;
    public registrationResult: CampRegistrationResult;

    ngOnInit(): void {
        this.createForm();
    }

    private createForm(): void {
        this.myForm = this.fb.group({
            name: ["", [Validators.required]],
            groupId: parseInt(this.route.snapshot.paramMap.get("campId")),
            levelId: 5
        })
    }

    public submitForm({ value, valid }: { value: any, valid: boolean }): void {
        this.saved = true;

        if (valid) {
            //Voer je actie uit 
            this.campService.registerToCamp(value).subscribe(result => this.registrationResult = result);
        }
    }
}
