import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Camp } from 'src/app/interfaces/camp';
import { CampService } from 'src/app/services/camp.service';

@Component({
    selector: 'camp-overview',
    templateUrl: 'camp-overview.component.html',
    styleUrls: ['./camp-overview.scss']
})

export class CampOverview implements OnInit {
    constructor(private campService: CampService,
        private router: Router) { }

    public camps: Camp[];

    ngOnInit() {
        this.loadData();
    }

    private loadData(): void {
        this.campService.getAllCamps().subscribe(camps => this.camps = camps);
    }

    public navigateToCampRegistration(id: number): void {
        this.router.navigate([`ktcb/registratie/${id}`]);
    }
}