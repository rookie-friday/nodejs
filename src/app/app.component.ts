import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
	selector: 'stage-billix',
	templateUrl: './app.component.html'
})

export class AppComponent implements OnInit {

	constructor(private router: Router, ){}

	public isOnIndex: boolean = true;

	ngOnInit():void {
		this.isOnIndex = true;
	}

	public navigateToPage(pageName: string): void {
        this.router.navigate([`/${pageName}`]);
		if(pageName != "index"){
			this.isOnIndex = false;
		}
    }

}